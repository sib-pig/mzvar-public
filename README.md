# **MzVar**

MzVar is a Java tool allowing the compilation of customized variant protein and peptide databases for database searching of MS/MS data.

To give us feedback or report a bug, please contact:
[Thibault.Robin@sib.swiss](mailto:Thibault.Robin@sib.swiss)

## **1 - Download**
The latest MzVar version can be downloaded from [this link](https://bitbucket.org/sib-pig/mzvar-public/downloads/mzvar.jar) or from the **Downloads** tab on the left of this page. If you intend to execute MzVar using command lines (*cf. 2.2*), you will also be asked to download the corresponding [Java properties file](https://bitbucket.org/sib-pig/mzvar-public/downloads/mzvar.properties) to be able to set the program parameters.

## **2 - Execution**
Java 8 or later is required for running MzVar. The latest Java version can be downloaded from the [oracle website](http://www.oracle.com/technetwork/java/javase/downloads/index.html).

### 2.1 - JavaFx Application  
By default, MzVar will launch as a JavaFx application when executing the packaged jar file. The graphical user interface allows to select all the needed input files and parameters. Note that every field has to be filled before being able to start the program.
  
  *Linux Warning* - If you encounter difficulties launching the application on a Linux platform, try first to install the openjfx library using the `sudo apt-get install openjfx` command line in a terminal.

###  2.2 - Command Lines  
As an alternative, MzVar can also be launched using the `java -cp mzvar.jar main.Main` command line. All the needed arguments can be found with the `-h` or `--help` option or by referring to the table below:


| Short Option   | Long Option    | Need Argument | Description                    |
|----------------|----------------|---------------|--------------------------------|
| `-g`           | `--gene`       | Yes           | *Transcript Sequence File Path*|
| `-v`           | `--variant`    | Yes           | *VCF Variant File Path*        |
| `-o`           | `--output`     | Yes           | *Output Folder Path*           |
| `-p`           | `--properties` | Yes           | *Property Parameter File Path* |
| `-e`           | `--ensembl`    | No            | *Sequence File from Ensembl*   |
| `-u`           | `--ucsc`       | No            | *Sequence File from UCSC*      |
| `-h`           | `--help`       | No            | *Arguments Formatting Help*    |

*Note that one of the two sequence file source must be specified (Ensembl or UCSC).*

Here is an example of a complete command line for an Ensembl file:  
 `java -cp mzvar.jar main.Main -g data/Sequences_GrCH38.fasta -v data/CCRF-CEM.vcf  -p mzvar.properties -o output/ -e` 

## **3 - Input Files**

###  3.1 - VCF File  
The variant input file has to be formatted in the Variant Call Format (VCF) 4.0 or later as described by the [1000 Genomes Project](http://www.internationalgenome.org/wiki/Analysis/Variant%20Call%20Format/vcf-variant-call-format-version-40/).

###  3.2 - Transcript  Sequence File 
The transcript sequences file can be downloaded either from the UCSC website through the [Table Browser tool](https://genome.ucsc.edu/cgi-bin/hgTables) or from the Ensembl website through the [Biomart tool](http://www.ensembl.org/biomart/). The following steps have to be completed to retrieve the sequences under the proper format:

#### [**UCSC TABLE BROWSER**](https://genome.ucsc.edu/cgi-bin/hgTables)

**Table Browser Window**

1. Select the right clade, genome and assembly. 
2. Select which gene reference to track.
3. Select *Genes and Genes predictions* as group.
4. Select *sequence* as output format. It is also highly recommended to put a file name under output file.

**Select Sequence Type Window**

1. Select *genomic*.

**Genomic Sequence Window**

1. Select *5'UTR Exons*.
2. Select *CDS Exons*.
3. Select *3'UTR Exons*.
4. Select *One FASTA record per region*.
5. Select *Split UTR and CDS parts of an exon into separate FASTA records* without extra bases.
4. Select *CDS in upper case, UTR in lower case*.
  
#### [**ENSEMBL BIOMART**](http://www.ensembl.org/biomart)

If you are using an older genome assembly than *GRCH38*, please select the correct one from [the archive link](http://www.ensembl.org/info/website/archives/index.html).

**Dataset Tab**

1. Select *Ensembl Genes* as Database.
2. Select the right Dataset.

**Attributes Tab**

1. Select *Sequences*.

**Attributes Tab - Sequences**

1. Select *cDNA sequences*.

**Attributes Tab -  Header Information**  

1. Select *Chromosome Name*.
2. Select *Transcript Strand*.
3. Select *Transcript Start (bp)*.
4. Select *Transcript End (bp)*.
5. Select *cDNA coding start*.
6. Select *cDNA coding end*.
7. Select *Genomic coding start*.
8. Select *Genomic coding end*.

*Note that the selected Attributes must be in this exact order to avoid header parsing errors.*
